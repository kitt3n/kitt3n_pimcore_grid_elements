pimcore.registerNS("pimcore.plugin.kitt3nPimcoreGridElementsBundle");

pimcore.plugin.kitt3nPimcoreGridElementsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.kitt3nPimcoreGridElementsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("kitt3nPimcoreGridElementsBundle ready!");
    }
});

var kitt3nPimcoreGridElementsBundlePlugin = new pimcore.plugin.kitt3nPimcoreGridElementsBundle();
