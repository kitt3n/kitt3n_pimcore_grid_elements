<?php

namespace KITT3N\Pimcore\GridElementsBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class kitt3nPimcoreGridElementsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcoregridelements/js/pimcore/startup.js'
        ];
    }
}